﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mine : MonoBehaviour
{
    public float force = 10;
    public float timeBeforeActive = .5f;
    public GameObject particlePrefab;
    
    void Start()
    {
        GetComponent<Rigidbody>().AddForce(force * Vector3.up);
    }

    public void Update()
    {
        timeBeforeActive -= Time.deltaTime;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (timeBeforeActive <= 0)
        {
            if (other.GetComponent<PlayerController>())
            {
                StartCoroutine(other.GetComponent<PlayerItemManager>().TakeMine());
                Instantiate(particlePrefab, transform.position, Quaternion.identity);
                Destroy(gameObject);
            }
        }
    }
}
