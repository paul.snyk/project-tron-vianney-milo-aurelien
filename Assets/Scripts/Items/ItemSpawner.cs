﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class ItemSpawner : MonoBehaviour
{
    public int itemIndex;
    
    bool spawned = true;
    public float respawnTime = 10;
    public GameObject cubeModel;

    private void Start()
    {
        spawned = true;
        itemIndex = Random.Range(0, 3);
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<PlayerController>() && spawned)
        {
            if (other.GetComponent<PlayerItemManager>().currentItem == -1)
            {
                FindObjectOfType<AudioManager>().Play("ItemPickUp");
                spawned = false;
                cubeModel.SetActive(false);
                other.GetComponent<PlayerItemManager>().currentItem = itemIndex;
            
                StartCoroutine(WaitBeforeRespawn());
            }
        }
    }

    IEnumerator WaitBeforeRespawn()
    {
        yield return new WaitForSeconds(respawnTime);
        itemIndex = Random.Range(0, 3);
        spawned = true;
        cubeModel.SetActive(true);
    }
}
