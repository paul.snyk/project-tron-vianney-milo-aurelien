﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public Transform[] spawnsTransforms;

    public CountParameter[] countParameter;

    public bool levelStart = false;

    //private GameController gameController;

    public int scoreToWin = 1;

    // Tron asset
    public TMP_FontAsset fontTron;

    // UI Game OVer
    public GameObject gameOverUI;
    public TMP_Text textWin;
    private bool gameHasEnded = false;

    [System.Serializable]
    public class CameraParameter
    {
        [Header("ViewportRect")] public Rect rect;
    }

    [System.Serializable]
    public class CountParameter
    {
        public CameraParameter[] cameraParameters;
    }

    public void SetupAllPlayers()
    {
        if (!levelStart)
        {
            levelStart = true;
            PlayerConnectedManager playerConnectedManager = GameController.instance.playerConnectedManager;

            // Setup player Camera
            for (int i = 0; i < playerConnectedManager.playerList.Count; i++)
            {
                PlayerController controller = playerConnectedManager.playerList[i].GetComponent<PlayerController>();
                controller.playerNumber = i;

                Camera camera = controller.camera;
                camera.rect = countParameter[GameController.instance.GetNbPlayer() - 2].cameraParameters[i].rect;

                camera.cullingMask = controller.layers[i];
                camera.gameObject.layer = 8 + i;
                controller.cinamachine.gameObject.layer = 8 + i;
                controller.particleSystem.gameObject.layer = 8 + i;

                controller.PlaceAtSpawn();
            }
        }
    }

    public void AddPoint(int playerNumber)
    {
        //gameController.playerConnectedManager.playerList[playerNumber].GetComponent<PlayerController>().score++;
        // Get player
        PlayerController player = GameController.instance.playerConnectedManager.playerList[playerNumber]
            .GetComponent<PlayerController>();

        // Add + 1 to the score's player
        player.score++;

        // Check if it's the score to win
        if (player.score >= scoreToWin)
        {
            Debug.Log("Enter into : if (playerNumber >= scoreToWin)");
            EndGame(playerNumber);
        }
    }

    public void EndGame(int playerNumber)
    {
        if (gameHasEnded == false)
        {
            FindObjectOfType<AudioManager>().Stop("MusicMenu");
            // Pause
            Time.timeScale = 0f;
            Debug.Log("Game Over");
            int numberPlayerToPrint = playerNumber + 1;
            gameOverUI.SetActive(true);
            textWin.text = "Player " + numberPlayerToPrint + " won";
            gameHasEnded = true;
        }
    }

    void Start()
    {
        GameController.instance = FindObjectOfType<GameController>();
    }
}