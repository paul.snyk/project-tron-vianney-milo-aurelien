﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public PlayerConnectedManager playerConnectedManager;
    public static GameController instance;
    // Players number
    public int nbPlayer;
    // Tron asset
    public TMP_FontAsset fontTron;
    // UI Game OVer

    void Update()
    {
        if (playerConnectedManager == null)
            if (FindObjectOfType<PlayerConnectedManager>())
            {
                playerConnectedManager = FindObjectOfType<PlayerConnectedManager>();
            }
    }

    void Awake()
    {
        if (instance == null){

            instance = this;
            DontDestroyOnLoad(gameObject);
        } 
        else 
        {
            Destroy(this);
        }
    }

    /*public void EndGame(int playerNumber)
    {
        if (gameHasEnded == false)
        {
            Debug.Log("Game Over");
            int numberPlayerToPrint = playerNumber + 1;
            
            if (gameOverUI != null)
            {
                gameOverUI.SetActive(true);
            }
            
            if (textWin != null)
            {
                textWin.text = "Player " + numberPlayerToPrint + " won";
            }
            gameHasEnded = true;
        }
    }*/

    public int GetNbPlayer()
    {
        return nbPlayer;
    }

    public void SetNbPlayer(int nb)
    {
        nbPlayer = nb;
    }
}
