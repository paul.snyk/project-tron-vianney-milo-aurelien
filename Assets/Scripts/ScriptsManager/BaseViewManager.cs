﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class BaseViewManager : MonoBehaviour
{
    public int numberOfPlayerNeeded;
    
    public TextMeshProUGUI[] player1Text;
    public TextMeshProUGUI[] player2Text;
    public TextMeshProUGUI[] player3Text;
    public TextMeshProUGUI[] player4Text;
    
    public GameObject twoPlayerObject;
    public GameObject threePlayerObject;
    public GameObject fourPlayerObject;

    public GameObject canvas;

    private PlayerConnectedManager playerConnectedManager;
    
    void Start()
    {
        playerConnectedManager = FindObjectOfType<PlayerConnectedManager>();
        if (GameController.instance != null)
            numberOfPlayerNeeded = GameController.instance.GetNbPlayer();
        
        switch (numberOfPlayerNeeded)
        {
            case 2:
                twoPlayerObject.SetActive(true);
                threePlayerObject.SetActive(false);
                fourPlayerObject.SetActive(false);
                break;
            case 3:
                twoPlayerObject.SetActive(false);
                threePlayerObject.SetActive(true);
                fourPlayerObject.SetActive(false);
                break;
            case 4:
                twoPlayerObject.SetActive(false);
                threePlayerObject.SetActive(false);
                fourPlayerObject.SetActive(true);
                break;
        }
    }
    
    
    void Update()
    {
        if (playerConnectedManager.playerCount >= 1)
        {
            foreach (var text in player1Text)
            {
                if (text != null)
                    text.text = "Player 1 Connected";
                    text.color = Color.green;
            }
        }
        
        if (playerConnectedManager.playerCount >= 2)
        {
            foreach (var text in player2Text)
            {
                if (text != null)
                    text.text = "Player 2 Connected";
                text.color = Color.green;
            }
        }
        
        if (playerConnectedManager.playerCount >= 3)
        {
            foreach (var text in player3Text)
            {
                if (text != null)
                    text.text = "Player 3 Connected";
                text.color = Color.green;
            }
        }
        
        if (playerConnectedManager.playerCount >= 4)
        {
            foreach (var text in player4Text)
            {
                if (text != null)
                    text.text = "Player 4 Connected";
                text.color = Color.green;
            }
        }
    }

    public void DeactivateView()
    {
        canvas.SetActive(false);
    }
}
