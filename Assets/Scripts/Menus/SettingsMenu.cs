﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Audio;
using UnityEngine;

public class SettingsMenu : MonoBehaviour
{
    // MainMixer
    public AudioMixer audioMixer;
    
    public void SetVolume(float volume)
    {
        audioMixer.SetFloat("volume", volume);
        if (volume <= -30)
        {
            // Force float to 0
            audioMixer.SetFloat("volume", -80);
        }
        else
        {
            // Set the exposed parameter "volume" of MainMixer
            audioMixer.SetFloat("volume", volume);
        }
    }

    public void SetFullscreen(bool isFullscreen)
    {
        // set fullscreen
        Screen.fullScreen = isFullscreen;
    }
}
