﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{

    void Start()
    {
        // Start the music menu
        FindObjectOfType<AudioManager>().Play("MusicMenu");
    }
    
    public void Play()
    {
        // Load the next scene
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void Quit()
    {
        // Quit Tron game
        Debug.Log("Quit Tron");
        Application.Quit();
    }
}
