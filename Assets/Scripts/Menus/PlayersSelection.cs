﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayersSelection : MonoBehaviour
{
    // call with buttons of player selection
    public void SetNbPlayer(int nb)
    {
        if (nb != null)
        {
            GameController.instance.SetNbPlayer(nb);
            Debug.Log("Nb of players = " + nb);
            Play();
        }
    }
    
    private void Play()
    {
        // Load the next scene
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
