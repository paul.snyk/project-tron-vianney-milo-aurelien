﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class PlayerItemManager : MonoBehaviour
{
    [Header("UI")]
    public Sprite[] itemSprites;

    public Image itemImage;
    public Image boxImage;

    public Sprite baseBoxSprite;
    public Sprite usingBoxSprite;

    [Header("Items parameters")]
    public int currentItem = -1;

    [Header("Speed boost")] 
    public float speedMultiplier = 1.5f;
    public float speedDuration = 4f;

    [Header("Shield")] 
    public GameObject shieldObject;
    public float shieldDuration = 5f;

    [Header("Blind mine")] 
    public GameObject minePrefab;
    public GameObject prefabHider;
    public Transform spawnTransform;
    public float blindDuration = 5;

    public Transform canvasTransform;
    private PlayerController playerController;
    private PlayerMovementController playerMovementController;
    private PlayerCollisionHandler playerCollisionHandler;
    
    private bool usingItem = false;
    
    
    void Start()
    {
        playerController = GetComponent<PlayerController>();
        playerMovementController = GetComponent<PlayerMovementController>();
        playerCollisionHandler = GetComponent<PlayerCollisionHandler>();
    }
    
    void Update()
    {
        if (currentItem != -1)
        {
            itemImage.color = new Color(1,1,1, 1);
            itemImage.sprite = itemSprites[currentItem];
            
            // USE ITEM
            if (playerController.iUse)
            {
                switch (currentItem)
                {
                    case 0:
                        StartCoroutine(SpeedBoost());
                        break;
                    case 1:
                        LaunchMine();
                        break;
                    case 2:
                        StartCoroutine(Shield());
                        break;
                }
            }
        }
        else
        {
            itemImage.color = new Color(1,1,1, 0);
        }

        if (usingItem)
            boxImage.sprite = usingBoxSprite;
        else
            boxImage.sprite = baseBoxSprite;
    }

    public IEnumerator SpeedBoost()
    {
        FindObjectOfType<AudioManager>().Play("UseSpeed");
        usingItem = true;
        playerMovementController.speedMultiplier = speedMultiplier;
        
        yield return new WaitForSeconds(speedDuration);
        
        playerMovementController.speedMultiplier = 1;
        
        usingItem = false;
        currentItem = -1;
    }

    public void LaunchMine()
    {
        Instantiate(minePrefab, spawnTransform.position, spawnTransform.rotation);
        
        currentItem = -1;
    }

    public IEnumerator Shield()
    {
        FindObjectOfType<AudioManager>().Play("UseShield");
        shieldObject.SetActive(true);    
        playerCollisionHandler.isProtected = true;
        usingItem = true;
        
        yield return new WaitForSeconds(shieldDuration);
        
        shieldObject.SetActive(false);
        playerCollisionHandler.isProtected = false;
        
        
        usingItem = false;
        currentItem = -1;
    }

    public IEnumerator TakeMine()
    {
        int hiderCount = Random.Range(4, 10);
        GameObject[] hiders = new GameObject[hiderCount];

        for (int i = 0; i < hiderCount; i++)
        {
            hiders[i] = Instantiate(prefabHider, canvasTransform);
            hiders[i].transform.localPosition += new Vector3(Random.Range((-1920/2)*playerController.camera.rect.width, (1920/2)*playerController.camera.rect.width), Random.Range((-1080/2)*playerController.camera.rect.height, (1080/2)*playerController.camera.rect.height), 0);
            hiders[i].transform.DOLocalRotate(new Vector3(0, 0, Random.Range(-180, 180)), blindDuration);
            hiders[i].GetComponent<Image>().DOFade(0, blindDuration);
        }
        yield return new WaitForSeconds(blindDuration);

        foreach (var hider in hiders)
        {
            Destroy(hider);
        }
    }
    
}
