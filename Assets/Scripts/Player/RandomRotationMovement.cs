﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomRotationMovement : MonoBehaviour
{
    public float maxSpeed = 50;
    public float angleChangeFactor = 0.1f;
    
    private float xAngleRotationSpeed;
    private float yAngleRotationSpeed;
    private float zAngleRotationSpeed;
    
    void Start()
    {
        xAngleRotationSpeed = Random.Range(-maxSpeed, maxSpeed);
        yAngleRotationSpeed = Random.Range(-maxSpeed, maxSpeed);
        zAngleRotationSpeed = Random.Range(-maxSpeed, maxSpeed);
    }
    
    void Update()
    {
        xAngleRotationSpeed +=
            Random.Range(-maxSpeed * angleChangeFactor, maxSpeed * angleChangeFactor) * Time.deltaTime;
        yAngleRotationSpeed +=
            Random.Range(-maxSpeed * angleChangeFactor, maxSpeed * angleChangeFactor) * Time.deltaTime;
        zAngleRotationSpeed +=
            Random.Range(-maxSpeed * angleChangeFactor, maxSpeed * angleChangeFactor) * Time.deltaTime;

        xAngleRotationSpeed = Mathf.Clamp(xAngleRotationSpeed, -maxSpeed, maxSpeed);
        yAngleRotationSpeed = Mathf.Clamp(yAngleRotationSpeed, -maxSpeed, maxSpeed);
        zAngleRotationSpeed = Mathf.Clamp(zAngleRotationSpeed, -maxSpeed, maxSpeed);
        
        transform.Rotate(xAngleRotationSpeed * Time.deltaTime, yAngleRotationSpeed * Time.deltaTime, zAngleRotationSpeed * Time.deltaTime);
    }
}
