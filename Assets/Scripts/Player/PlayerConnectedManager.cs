﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.UI;

public class PlayerConnectedManager : MonoBehaviour
{
    public int playerCount = 0;

    public List<PlayerInput> playerList;

    private PlayerInputManager playerInputManager;

    public InputSystemUIInputModule uiInputModule;
    
    void OnPlayerJoined(PlayerInput playerInput)
    {
        
        playerCount++;
        playerList.Add(playerInput);
        
        if (playerCount == GameController.instance.GetNbPlayer())
            StartCoroutine(StartGame());
    }
    

    void OnPlayerLeft(PlayerInput playerInput)
    {
        playerCount--;
        
        playerList.Remove(playerInput);
    }

    IEnumerator StartGame()
    {
        FindObjectOfType<LevelManager>().SetupAllPlayers();
        yield return new WaitForSeconds(.5f);

        for (int i = 0; i < playerList.Count; i++)
        {
            playerList[i].GetComponent<PlayerController>().StartMoving();
        }

        FindObjectOfType<BaseViewManager>().DeactivateView();
        
    }
}
