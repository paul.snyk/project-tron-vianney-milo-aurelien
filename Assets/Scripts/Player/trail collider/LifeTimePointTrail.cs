﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeTimePointTrail : MonoBehaviour
{
    public float lifeTimer;

    // Update is called once per frame
    void Update()
    {
        lifeTimer -= Time.deltaTime;
        if (lifeTimer < 0)
        {
            Destroy(this.gameObject);
        }
    }
}
