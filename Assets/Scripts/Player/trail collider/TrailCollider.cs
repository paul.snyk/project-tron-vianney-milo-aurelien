﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrailCollider : MonoBehaviour
{
    public GameObject folderCollider;
    public GameObject colliderBlock;

    public Vector3 lastPlayerPosition;
    public Vector3 newPlayerPosition;

    public List<GameObject> trails;

    private bool active;

    // Start is called before the first frame update
    void Start()
    {
        lastPlayerPosition = gameObject.transform.position;
    }

    private void Update()
    {
        if (active)
        {
            newPlayerPosition = gameObject.transform.position;
            float distance = Vector3.Distance(lastPlayerPosition, newPlayerPosition);
            //Debug.Log("distance = " + distance);
            if (distance >= 1)
            {
                trailColliderInstance(lastPlayerPosition,gameObject,colliderBlock);
            
                lastPlayerPosition = newPlayerPosition;
            }
            //trailColliderInstance(trailPoint.transform.position,gameObject,colliderBlock);
        }
    }

    public void DestroyTrail()
    {
        foreach (var trail in trails)
        {
            Destroy(trail);
        }
        trails.Clear();
    }

    public void ActiveTrail()
    {
        active = true;
        lastPlayerPosition = gameObject.transform.position;
    }

    public void DeactivateTrail()
    {
        active = false;
    }

    public void trailColliderInstance(Vector3 a, GameObject player, GameObject trailPrefab)
    {
        Vector3 b = player.transform.position;
        
        GameObject newTrailCollider = Instantiate(trailPrefab, folderCollider.transform);
        newTrailCollider.layer = 13 + GetComponent<PlayerController>().playerNumber;
        trails.Add(newTrailCollider);
        
        newTrailCollider.transform.position = a;
        
        newTrailCollider.transform.LookAt(player.transform, player.transform.up);
    }
}
