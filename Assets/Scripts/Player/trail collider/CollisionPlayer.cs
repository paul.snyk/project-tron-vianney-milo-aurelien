﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionPlayer : MonoBehaviour
{
    private CharacterController controller;
    // Start is called before the first frame update
    void Start()
    {
        controller = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnControllerColliderHit(ControllerColliderHit hit)
    {
        Debug.Log("oui collision");
        if (hit.gameObject.CompareTag("ColliderPlayer1"))
        {
            Debug.Log(gameObject.name + " touche le " + hit.gameObject.name);
        }
    }
}
