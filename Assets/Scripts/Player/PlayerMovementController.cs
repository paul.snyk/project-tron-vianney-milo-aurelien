﻿using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerMovementController : MonoBehaviour
{
    [Header("Movement")]
    public float maxSpeed = 50f;
    public float acceleration = 10f;
    public float gravity = -9.8f;

    [Header("Rotation")]
    public float rotationSpeed = 15f;
    public float sideRotateSpeed = .1f;
    public float sideRotateForce = 40f;

    [Header("Speed effects")] 
    public float particleMaxEmissionRate = 200;
    public float particleMinSpeed = 25;
    public float particleMaxSpeed = 50;

    public float baseFollowOffset = -5;
    public float maxFollowOffset = -10;

    public float baseFOV = 30;
    public float maxFOV = 95;

    [Header("UI")]
    public TextMeshProUGUI countDownText;
    bool moving;

    public CinemachineVirtualCamera virtualCamera;
    public ParticleSystem particleSystem;
    public Transform model;
    
    [Header("Items Modifiers")]
    public float speedMultiplier = 1;
    
    
    private PlayerController playerController;
    private CharacterController characterController;
    CinemachineTransposer cinemachineTransposer;
    
    private float forwardSpeed;
    private float verticalSpeed;

    public bool canPlayMusic = true;

    void Start()
    {
        characterController = GetComponent<CharacterController>();
        countDownText.font = GameController.instance.fontTron;
        playerController = GetComponent<PlayerController>();
        cinemachineTransposer = virtualCamera.GetCinemachineComponent<CinemachineTransposer>();

        particleSystem.startSpeed = 0;
        
        var em = particleSystem.emission;
        em.rateOverTime = 0;
    }

    void Update()
    {
        if (moving)
        {
            forwardSpeed += acceleration * speedMultiplier * Time.deltaTime;
            forwardSpeed = Mathf.Clamp(forwardSpeed, 0, maxSpeed*speedMultiplier);
            
            // Check for rotate
            if (playerController.iTurn != Vector2.zero && characterController.isGrounded)
            {
                transform.Rotate(new Vector3(0, playerController.iTurn.x * rotationSpeed * Time.deltaTime, 0));
            }
            model.transform.DOLocalRotate(new Vector3(0, 0, sideRotateForce * -playerController.iTurn.x), sideRotateSpeed,
                RotateMode.Fast);

            // Check for radicalTurn
            if (playerController.iRadicalTurn != 0 && characterController.isGrounded)
            {
                transform.Rotate(new Vector3(0, playerController.iRadicalTurn * 90,0));
                playerController.iRadicalTurn = 0;
            }
            
            // Apply Gravity
            verticalSpeed += gravity * Time.deltaTime;
            
            // Move
            characterController.Move((transform.forward * forwardSpeed + transform.up * verticalSpeed) * Time.deltaTime);
            
            // Reset velocity on Ground
            if (characterController.isGrounded)
            {
                verticalSpeed = 0;
            }

            // Set particle effect
            if (forwardSpeed > 15)
            {
                particleSystem.startSpeed = ((forwardSpeed - 15) / maxSpeed) * (particleMaxSpeed-particleMinSpeed) + particleMinSpeed;
                
                var em = particleSystem.emission;
                em.rateOverTime = (forwardSpeed - 15) / maxSpeed * particleMaxEmissionRate;
            }

            if (forwardSpeed > 0)
            {
                cinemachineTransposer.m_FollowOffset = new Vector3(cinemachineTransposer.m_FollowOffset.x, cinemachineTransposer.m_FollowOffset.y, (forwardSpeed / maxSpeed) * (maxFollowOffset-baseFollowOffset) + baseFollowOffset);
                virtualCamera.m_Lens.FieldOfView = (forwardSpeed / maxSpeed) * (maxFOV - baseFOV) + baseFOV;
            }
        }
    }

    public IEnumerator StartMoving()
    {
        cinemachineTransposer.m_FollowOffset = new Vector3(cinemachineTransposer.m_FollowOffset.x, cinemachineTransposer.m_FollowOffset.y, baseFollowOffset);
        virtualCamera.m_Lens.FieldOfView = baseFOV;
        
        FindObjectOfType<AudioManager>().Play("Start");
        countDownText.text = "3";
        yield return new WaitForSeconds(1);
        FindObjectOfType<AudioManager>().Play("Start");
        countDownText.text = "2";
        yield return new WaitForSeconds(1);
        FindObjectOfType<AudioManager>().Play("Start");
        countDownText.text = "1";
        yield return new WaitForSeconds(1);
        if (canPlayMusic)
        {
            FindObjectOfType<AudioManager>().Play("MusicMenu");
            canPlayMusic = false;
        }
        
        countDownText.text = "GO";
        
        playerController.trail.emitting = true;
        
        moving = true;
        // FindObjectOfType<AudioManager>().Play("MotoSound");
        
        yield return new WaitForSeconds(2);
        countDownText.text = "";
    }

    public void StopMoving()
    {
        // FindObjectOfType<AudioManager>().Stop("MotoSound");
        moving = false;
        forwardSpeed = 0;
        verticalSpeed = 0;
        
        model.rotation = Quaternion.identity;

        particleSystem.startSpeed = 0;
        
        var em = particleSystem.emission;
        em.rateOverTime = 0;
    }
}
