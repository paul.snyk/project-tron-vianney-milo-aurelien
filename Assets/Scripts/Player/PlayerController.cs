﻿using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.UI;
using UnityEngine.Rendering.Universal;

public class PlayerController : MonoBehaviour
{
    public int playerNumber;
    
    public Vector2 iTurn;
    public bool iUse = false;
    public float iRadicalTurn;

    public bool asStart;

    public TrailRenderer trail;
    public CinemachineVirtualCamera cinamachine;
    public Camera camera;
    public ParticleSystem particleSystem;
    public GameObject canvas;
    
    public LayerMask[] layers;

    public Transform spawn;

    public TextMeshProUGUI scoreText;
    public int score = 0;

    public PlayerCustomisation[] customisations;
    
    private PlayerMovementController playerMovementController;
    private TrailCollider trailCollider;

    [System.Serializable]
    public class PlayerCustomisation
    {
        [ColorUsageAttribute(true,true,0f,8f,0.125f,3f)]
        public Color colorTrail;
        public GameObject bikeModel;
    }
    
    
    void Start()
    {
        trail.emitting = false;
        trail.time = 0;
        trailCollider = GetComponent<TrailCollider>();
        trailCollider.DeactivateTrail();
        
        canvas.SetActive(false);
        playerMovementController = GetComponent<PlayerMovementController>();
    }

    void Update()
    {
        if (canvas.activeSelf)
        {
            scoreText.text = "Score : " + score;
        }
    }

    public void InitBikeAndColor()
    {
        trail.material.SetColor("_EmissionColor", customisations[playerNumber].colorTrail);
        customisations[playerNumber].bikeModel.SetActive(true);
    }

    public void PlaceAtSpawn()
    {
        spawn = FindObjectOfType<LevelManager>().spawnsTransforms[playerNumber];
        InitBikeAndColor();
        
        transform.position = spawn.position;
        transform.rotation = spawn.rotation;
    }

    public void StartMoving()
    {
        canvas.SetActive(true);
        trail.time = 10;
        trailCollider.ActiveTrail();
        StartCoroutine(playerMovementController.StartMoving());
    }
    
    private void OnTurn(InputValue value)
    {
        iTurn = value.Get<Vector2>();
    }
    
    private void OnUse(InputValue value)
    {
        iUse = value.Get<float>() > 0;
    }
    
    private void OnRadicalTurn(InputValue value)
    {
        iRadicalTurn = value.Get<float>();
    }
    
    
}
