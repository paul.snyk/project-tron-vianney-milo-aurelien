﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollisionHandler : MonoBehaviour
{
    public LayerMask layerToCollideWith;
    public float minAngleToCollide = 10;

    public GameObject sparkParticle;
    public GameObject deathParticle;

    public float RespawnTime = 5f;

    public bool isProtected;

    private PlayerController playerController;
    private PlayerMovementController playerMovementController;


    private void Start()
    {
        playerController = GetComponent<PlayerController>();
        playerMovementController = GetComponent<PlayerMovementController>();
    }

    private void Update()
    {
        if (transform.position.y < -50)
        {
            Die();
            transform.position += Vector3.up * 3;
        }
    }

    public void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if (layerToCollideWith == (layerToCollideWith | (1 << hit.collider.gameObject.layer)))
        {
            if (Vector3.Angle(hit.normal, transform.forward)-90 > minAngleToCollide)
            {
                if (!isProtected)
                {
                    if (hit.collider.gameObject.layer - 13 != playerController.playerNumber && hit.collider.gameObject.layer != 12)
                    {
                        FindObjectOfType<LevelManager>().AddPoint(hit.collider.gameObject.layer - 13);
                    }
                
                    Die();
                }
                else
                {
                    Destroy(Instantiate(sparkParticle, hit.point, Quaternion.identity), .6f);
                }
            }
            else
            {
                Destroy(Instantiate(sparkParticle, hit.point, Quaternion.identity), .6f);
            }
        }
    }

    public void Die()
    {
        FindObjectOfType<AudioManager>().Play("Explosion");
        Destroy(Instantiate(deathParticle, transform.position, Quaternion.identity), 5f);
        playerMovementController.StopMoving();
        playerMovementController.model.gameObject.SetActive(false);
        
        playerController.score--;
        if (playerController.score < 0) playerController.score = 0;

        playerController.trail.emitting = false;
        playerController.trail.time = 0;
        GetComponent<TrailCollider>().DeactivateTrail();
        GetComponent<TrailCollider>().DestroyTrail();

        GetComponent<PlayerItemManager>().currentItem = -1;

        StartCoroutine(WaitForRespawn(RespawnTime));
    }

    public IEnumerator WaitForRespawn(float time)
    {
        yield return new WaitForSeconds(time);
        
        playerMovementController.model.gameObject.SetActive(true);
        playerController.PlaceAtSpawn();
        playerController.StartMoving();
    }
}
