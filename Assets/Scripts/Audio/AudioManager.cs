﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Audio;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    // Sounds array
    public Sound[] sounds;

    // AudioMixerGroup
    public AudioMixerGroup audioMixerGroup;
    

    void Awake()
    {
        // Update each sound
        foreach (Sound sound in sounds)
        {
            // Add the AudioSource component in AudioManager
            sound.source = gameObject.AddComponent<AudioSource>();

            // Update the AudioSource
            sound.source.clip = sound.clip;
            sound.source.volume = sound.volume;
            sound.source.pitch = sound.pitch;
            sound.source.loop = sound.loop;
            sound.source.outputAudioMixerGroup = audioMixerGroup;
        }
    }

    public Sound GetSound(string name)
    {
        // Find the sound with his name, by the name in parameter
        Sound sound = Array.Find(sounds, s => s.name == name);

        return sound;
    }

    public void Play(string name)
    {
        Sound sound = GetSound(name);
        if (sound != null)
        {
            // Play the sound
            sound.source.Play();
        }
        else
        {
            Debug.LogWarning("Error, no sounds found with the name '" + name + "' ");
        }
    }

    public void Stop(string name)
    {
        Sound sound = GetSound(name);
        if (sound != null)
        {
            // Stop the sound
            sound.source.Stop();
        }
        else
        {
            Debug.LogWarning("Error, no sounds found with the name '" + name + "' ");
        }
    }
}