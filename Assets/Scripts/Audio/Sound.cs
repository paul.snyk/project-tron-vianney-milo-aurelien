﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Sound
{
    // Sound name
    public string name;
    
    // Audio clip
    public AudioClip clip;

    [Range(0f, 1f)]
    // Volume
    public float volume;
    
    [Range(.1f, 3f)]
    // Sound pitch
    public float pitch;

    [HideInInspector]
    // Audio source (hide)
    public AudioSource source;

    // Sound loop
    public bool loop;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
